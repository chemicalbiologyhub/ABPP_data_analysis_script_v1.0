# ABPP Data Analysis Script V1.0

R script and data for reproducible analysis of ABPP-MS data

This project contains the raw data output from FragPipe and an R script for mapping peptide modifications to protein sequences and summarizing labelling events across replicates and collections of peptides.

Computes a competition ratio based on the intensity of enrichment and competition samples against ioda-acetamide.

Written by Elizabeth Rothweiler and Jeppe Tranberg-Jensen

Kilian Huber group, University of Oxford, 02/11 2023


The script was written using Rstudio v. 2023.6.2.561 and R v. 4.3.1 


The script relies on the following R packages:

Data extraction:

library(BiocManager)

library(UniProt.ws)

library(httr)

library(jsonlite)


Viz:

library(ggplot2)

library(ggVennDiagram)

library(viridis)


Stats:

library(Hmisc)


Data wrangling:

library(data.table)

library(stringr)

library(purrr)

library(tidyr)

library(dplyr)

